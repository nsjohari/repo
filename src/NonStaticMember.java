
public class NonStaticMember {
	
	int a=40;  //non static

	public static void main(String[] args) {
		
		NonStaticMember x = new NonStaticMember();
		
		
		System.out.println(x.a);
		
		/*
		 If this variable is static then we need not to make any object
		 and we can directly call like this
		 
		 static int a =40;
		 Syatem.out.println(NonStaticMember.a);
		 */

	}

}
