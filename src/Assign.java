//import java.util.Scanner;
public class Assign {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//////////////////////////////////		
		//Q1.....
		//System.out.println("Hello World");
		
////////////////////////////////////////////////////////////////
		/* Q2.........
		int a = 10;
		float b = 12.5f;
		String c = "Java Programming";
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		*/
//////////////////////////////////////////////////////////////////////		
//Please change the case to see the different results
		/* Q3............
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number 1");
		int a = sc.nextInt();
		System.out.println("Enter Number 2");
		int b = sc.nextInt();
		
		int sum = a +b;
		int substraction = a-b;
		int multiplication = a * b;
		int divide = a/b;
		int module = a % b;
		
		char symbol = '+';
		switch(symbol) 
		{
		case '+':
			System.out.println("The sum of these numbers is");
			System.out.println(sum);
			break;
		case '-':
			System.out.println("The substraction of these numbers is");
			System.out.println(substraction);
			break;
		case '*':
			System.out.println("The multiplication of these numbers is");
			System.out.println(multiplication);
			break;
		case '/':
			System.out.println("The divide of these numbers is");
			System.out.println(divide);
			break;
		case '%':
			System.out.println("The module of these numbers is");
			System.out.println(module);		
		
		}
		
		*/
		
//////////////////////////////////////////////////////////////////////////////		
////If you want to print all values together then you can run this program
		
		/*Q3...................
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number 1");
		int a = sc.nextInt();
		System.out.println("Enter number 2");
		int b = sc.nextInt();
		
		int sum = a +b;
		int substraction = a-b;
		int multiplication = a * b;
		int divide = a/b;
		int module = a % b;
		
		System.out.println("The sum of these numbers is");
		System.out.println(sum);
		
		System.out.println("The substraction of these numbers is");
		System.out.println(substraction);
		
		System.out.println("The multiplication of these numbers is");
		System.out.println(multiplication);
		
		System.out.println("The divide of these numbers is");
		System.out.println(divide);
		
		System.out.println("The module of these numbers is");
		System.out.println(module);
		
		*/
		
////////////////////////////////////////////////////////////////////////////		
	/*Q4............
		System.out.println("Enter any number:");
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		
		if(num%2==0) {
			System.out.println(num+" is an even number");			
		}
		else {
			System.out.println(num+" is an odd number");
		}
		*/
/////////////////////////////////////////////////////////////////////		
	/*Q5................
		System.out.println("Enter your age: ");
		Scanner sc=new Scanner(System.in);
		
		int age=sc.nextInt();
		
		if(age>=18) {
			System.out.println("You are eligible to vote");
		}
		else {
			System.out.println("You are not eligible to vote");
		}
		*/
////////////////////////////////////////////////////////////////////		
	//Q6
		/*
		System.out.println("Enter a number to check it's sign");
		Scanner sc=new Scanner(System.in);
		int num= sc.nextInt();
		
		if(num<0) {
			System.out.println("Given number is negative");
		}
		else {
			System.out.println("Given number is positive");
		}
	*/
/////////////////////////////////////////////////////////////////////	
	///Q7
		/*
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter a Number: ");
		
		int num=sc.nextInt();
		
		int rev =0;
		
		while(num!=0) {
			
			rev=rev*10 + num%10;
			num=num/10;
		}
		System.out.println("Reverse Number is: "+ rev);
		*/
///////////////////////////////////////////////////////////////////////////		
	///////Q8
		/*
		Scanner sc=new Scanner(System.in); 
		System.out.println("Enter the First number: ");
		int num1 = sc.nextInt();
		
		System.out.println("Enter the Second number: ");
		int num2 = sc.nextInt();
		
		System.out.println("Enter the Third number: ");
		int num3 = sc.nextInt();
		
		if(num1>num2 && num1>num3) {
			System.out.println("The greatest num is: "+ num1);
			
		}else if(num2>num3 && num2>num1) {
			System.out.println("The greatest num is: "+ num2);
			
		}else if(num3>num2 && num3>num1) {
			System.out.println("The greatest num is: "+ num3);
		}
		*/
////////////////////////////////////////////////////////////////////
	///////Q9
		/*
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number: ");
		int num=sc.nextInt();
		
		int rev=0;
		
		int org_num=num;
		
		while(num!=0) {
		
		rev=rev*10 + num%10;
		num=num/10;
		}
		if(org_num==rev) {
			System.out.println(org_num+ " is a Palindrome number");
		}else {
			System.out.println(org_num+ " is not a Palindrome number");
		}
		*/
///////////////////////////////////////////////////////////////////////////	
///////////Q10	
	/*
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter your score in %");
		
		int score=sc.nextInt();
		
		if(score>90) {
			System.out.println("Your Grade is A");
		}else if(score>70 && score<90) {
			System.out.println("Your Grade is B");
		}else if(score>50 && score<70) {
			System.out.println("Your Grade is C");
		}else {
			System.out.println("Your Grade is F");
		}
		*/
////////////////////////////////////////////////////////////////////////
///////////Q11		
		/*
		int a=1;
		while(a<=10) {
			System.out.println(a);
			a++;
		}
		*/
////////////////////////////////////////////////////////////////////				
//////////////Q12
		/*
		int i;
		for(i=10;i>=1;i--) {
			System.out.println(i);	
		}
		*/
////////////////////////////////////////////////////////////////////////	
////////////Q13	
		/*
		int a=1;
		while(a<=10) {
			System.out.println(a);
		}
		*/
//////////////////////////////////////////////////////////////////////////	
//////////Q14
		/*
		int i;
		for(i=1;1<=10;) {
			System.out.println(i);
		}
		*/
/////////////////////////////////////////////////////////////////////		
/////////////Q15
		/*
		int i;
		for(i=2;i<=20;i++){		
				if(i%2==0){
			System.out.println(i);
				}
		}
		*/
/////////////////////////////////////////////////////////////		
/////////////Q16	
		/*
		int i;
		for(i=1;i<=19;i++) {
			if(i%2!=0) {
				System.out.println(i);
			}
		}
		*/
////////////////////////////////////////////////////////////////	
//////////////Q17-for loop
		/*
		for(int i=0; i<100; i++) {
			System.out.println(i);
			if(i==9) {
				break;
			}
			
		}
		*/
////////////////////////////////////////////////////////////////////		
/////////////////Q17-while loop	
		/*
		int a=1;
		while(a<=100) {
			System.out.println(a);
			a++;
			if(a==12) {
				break;
			}
		}
		*/
/////////////////////////////////////////////////////////////////////		
///////////////////Q18-for loop	
		/*
		for(int i=1;i<100;i++) {			
			if(i==13) {
				continue;
			}
			System.out.println(i);
		}
		*/
////////////////////////////////////////////////////////////////////	
///////////////Q18-while loop
		/*
		int a =0;
		while(a<=100) {
			a++;
			if(a==13) {
				continue;
				}
				System.out.println(a);
		}
		*/
////////////////////////////////////////////////////////////////////		
		
	
		
		
		
		
		
			
		}
	}


