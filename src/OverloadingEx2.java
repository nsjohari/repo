
public class OverloadingEx2 {
	void sum(int a,int b) {System.out.println(a+b);}
	void sum(double a,double b) {System.out.println(a+b);}

	public static void main(String[] args) {
		OverloadingEx2 obj=new OverloadingEx2();
		obj.sum(11.5, 17.5);
		obj.sum(20, 40);

	}

}
