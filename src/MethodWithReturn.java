
public class MethodWithReturn {
	
	int add(int a, int b) {
		int sum=a+b;
		return sum;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MethodWithReturn ob=new MethodWithReturn();
		int x=ob.add(3, 4);
		int x1=ob.add(33, 44);
		int x2=ob.add(333, 444);
		int x3=ob.add(30, 40);
		int x4=ob.add(300, 400);
		int summary=x+x1+x2+x3+x4;
		
		System.out.println(x);
		System.out.println(x1);
		System.out.println(x2);
		System.out.println(x3);
		System.out.println(x4);
		System.out.println(summary);

	}

}
