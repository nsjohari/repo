
public class StaticVar {
	int rollno;
	String name;
	static String college = "ITS";
	
	StaticVar(int r, String n){
		rollno =r;
		name=n;
	}
	
	void display () {System.out.println(rollno+" "+name+" "+college);}

	public static void main(String[] args) {
		StaticVar s1 = new StaticVar(111,"Karan");
		StaticVar s2 = new StaticVar(222,"Aryan");
		StaticVar s3 = new StaticVar(223,"Aryan1");
		StaticVar s4 = new StaticVar(224,"Aryan2");
		StaticVar s5 = new StaticVar(225,"Aryan3");
		
		
		s1.display();
		s2.display();
		s3.display();
		s4.display();
		s5.display();

	}

}
